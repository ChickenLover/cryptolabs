#!/usr/bin/env python3

import socket
import hashlib

def s_key_secure_hash(msg):
    hash = hashlib.md5(msg).hexdigest()
    part_0 = int(hash[0:8], 16)
    part_1 = int(hash[8:16], 16)
    part_2 = int(hash[16:24], 16)
    part_3 = int(hash[24:32], 16)
    
    part_0 ^= part_2
    part_1 ^= part_3

    out = (part_0 << 32) | part_1
    out = out.to_bytes((out.bit_length() + 7) // 8, byteorder='little')
    
    return out

def main():
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_sock.connect(('127.0.0.1', 12345))

    login = input("Введите логин:")
    client_sock.sendall(login.encode())
    resp = client_sock.recv(1024).decode()

    if "Unknown user" in resp:
        print("Пользователь не найден")
        return
    elif resp:
        seed, seq = resp.strip().split()
        print(f'Recieved seq {seq}')
        passwd = input("Введите пароль:")

        hsh = seed + passwd
        hsh = s_key_secure_hash(hsh.encode())
        for i in range(int(seq)):
            hsh = s_key_secure_hash(hsh)
        client_sock.sendall(hsh)

        resp = client_sock.recv(1024).decode()
        if "Connection established" in resp:
            print("Соединение с сервером уставновлено")
            s = input("Ваше сообщение серверу:")
            client_sock.sendall(s.encode())

            resp = client_sock.recv(1024).decode()
            print(f"Ответ сервера: {resp}")
        elif "Wrong password" in resp:
            print("Неправильный пароль")
            return
    else:
        print("Соединение не установлено!")
        return

if __name__ == '__main__':
    main()
