#!/usr/bin/env python3

import os
import sys
import json
import socket
import base64
import hashlib
import string
from pprint import pprint


def hashstep(msg):
    hashed = hashlib.md5(msg).hexdigest()
    part_0 = int(hashed[0:8], 16)
    part_1 = int(hashed[8:16], 16)
    part_2 = int(hashed[16:24], 16)
    part_3 = int(hashed[24:32], 16)
    part_0 ^= part_2
    part_1 ^= part_3
    out = (part_0 << 32) | part_1
    out = out.to_bytes((out.bit_length()+7) // 8, 'little')
    return out

def hash_func(msg, seqn):
    hsh = hashstep(msg.encode())
    for _ in range(seqn):
        hsh = hashstep(hsh)
    return hsh

# Загрузить базу данных из файла
def load_db():
    if not os.path.exists('db.json'):
        with open('db.json', 'w') as f:
            json.dump({'users': {}}, f)
    with open('db.json') as f:
        return json.load(f)

# Сохранить базу данных в файл
def save_db(db):
    with open('db.json', 'w') as f:
        json.dump(db, f)

def reinit():
    pass

def process_connection(connection):
    print(f'Recieved connection from {connection}')
    db = load_db()
    user = connection.recv(1024).decode().strip()
    print(f'Looking for "{user}"')
    userdata = db['users'].get(user)
    if  userdata is None:
        connection.send(b"Unknown user\n")
        connection.close()
        return

    connection.send("{} {}\n".format(userdata["seed"], userdata["seq"]).encode())
    pre_hash = connection.recv(1024)
    hashres = hashstep(pre_hash)
    if hashres == base64.b64decode(userdata["hash"]):
        seq = int(userdata["seq"])-1
        if seq == -1:
            reinit()
        else:
            userdata["seq"] = seq
            userdata["hash"] = base64.b64encode(pre_hash).decode()
        connection.send(b"Connection established")
        msg = connection.recv(1024).decode()
        print(f"[{user}]: Recieved {msg}")
        connection.send(("Server recieved from user:" + msg).encode())
    else:
        connection.send("Wrong password".encode())
    db['users'][user] = userdata
    print(userdata)
    save_db(db)

def start_and_serve():
    listener = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    listener.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    IP = "127.0.0.1"
    PORT = 12345
    listener.bind((IP,PORT))
    listener.listen(0)
 
    print('Server started. Waiting for connections...')
    while True:
        try:
            connection, _ = listener.accept()
            process_connection(connection)
        except Exception as e:
            print(e)
            continue

def add_user(user, password, seed, seq):
    hsh = hash_func(str(seed) + password, int(seq))
    db = load_db()
    db['users'][user] = {
        "hash": base64.b64encode(hsh).decode(),
        "seed": seed,
        "seq": int(seq) - 1,
    }
    save_db(db)

def main():
    if len(sys.argv) < 2:
        sys.exit('Please provide a command "./sever.py add" or "./server.py start"')
    command = sys.argv[1]
    if command == "add":
        if len(sys.argv) < 6:
            sys.exit('Wrong number of arguments. Usage "./server.py add <user> <password> <seed> <sequence number>"')
        add_user(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    elif command == "start":
        start_and_serve()
    elif command == "show":
        db = load_db()
        pprint(db)
    else:
        sys.exit("Unknown command")

if __name__ == '__main__':
    main()
