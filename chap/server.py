#!/usr/bin/env python3

import socket
import hashlib
import string
import random
import time

secret = "password"
letters = string.ascii_letters + string.digits

class ChapPacket:
    CHALLENGE = 1
    RESPONSE = 2
    SUCCESS = 3
    FAILURE = 4

    def __init__(self, p_bytes, hsh=None):
        self.p_bytes = p_bytes
        if hsh:
            self.hsh = hsh

    @classmethod
    def create_challenge(cls, p_id):
        identifier = chr(p_id)
        r = random.randint(1, 16)
        value = "".join(random.choice(letters) for _ in range(r))
        length = chr(4 + r)
        s = str(cls.CHALLENGE) + identifier + length + chr(r) + value
        answer = identifier + secret + value
        hsh = hashlib.md5(answer.encode()).digest()

        #print(f"Created challenge, value = {value}")
        #print(f"Answer hash = {hsh}")

        return ChapPacket(s, hsh)

    @classmethod
    def create_success(cls, p_id, msg=""):
        identifier = chr(p_id)
        length = chr(3+len(msg))
        s = str(cls.SUCCESS) + identifier + length + msg
        return ChapPacket(s)

    @classmethod
    def create_failure(cls, p_id, msg=""):
        identifier = chr(p_id)
        length = chr(3+len(msg))
        s = str(cls.FAILURE) + identifier + length + msg
        return ChapPacket(s)

    def encode(self):
        return self.p_bytes.encode()
        

def challenge_check(pi, response, hsh):
    resplen = len(response)
    if      resplen > 2\
        and response[0] == ord('2')\
        and response[1] == pi\
        and int.from_bytes(response[2:3], 'little') == resplen:
        valuesize = response[4]
        responsevalue = response[5:(5+valuesize)]
        return hsh == responsevalue


def process_connection(connection):
    print(f'Accepted connection from {connection}')
    pi = 0
    start = time.time()
    interval = 0
    while True:
        t = time.time() - start
        if t % 1 == 0:
            print(t)
        if t >= interval:
            fc = 1
            packet = ChapPacket.create_challenge(pi)
            connection.send(packet.encode())
            print("Challenge sent")

            response = connection.recv(1024)
            if challenge_check(pi, response, packet.hsh):
                connection.send(ChapPacket.create_success(pi).encode())
                data = connection.recv(1024).decode()
                if data != None:
                    print("Client:" + data)
                    connection.send("OK".encode())
            else:
                print("Client sent wrong value")
                connection.send(ChapPacket.create_failure(pi).encode())
                connection.close()
                break
            pi = (pi + 1) % 256
            interval = random.randint(3, 10)
            start = time.time()


def main():
    listener = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    listener.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    IP = "127.0.0.1"
    PORT = 12345
    listener.bind((IP,PORT))
    listener.listen(0)

    print('Server started. Waiting for connections...')
    while True:
        #try:
        connection,address = listener.accept()
        process_connection(connection)
        #except Exception as e:
        #    print(e)
        #    print("Something went wrong(Connection closed)")
        #    continue

if __name__ == '__main__':
    main()
