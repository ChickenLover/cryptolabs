#!/usr/bin/env python3

import time
import socket
import hashlib

IP = "127.0.0.1"
PORT = 12345

def md5(msg):
    return hashlib.md5(msg).digest()

def solve_challenge(secret, resp):
    identifier = resp[1:2]
    len_data = int.from_bytes(resp[3:4], 'little')
    value = resp[4:4+len_data]

    hsh = md5(identifier + secret + value)

    length = (5 + len(hsh)).to_bytes(2, 'little')
    hash_size = chr(len(hsh)).encode()
    
    return b'2' + identifier + length + hash_size + hsh

def main():
    secret = b"password"

    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_sock.connect((IP, PORT))

    try:
        while True:
            resp = client_sock.recv(1024)
            print(f"Ответ от сервера: {resp.decode()}")

            if (resp.decode()[0] == '1') and len(resp)>=3:
                print("Решаем challenge")
                solve = solve_challenge(secret, resp)
                client_sock.sendall(solve)

            elif resp.decode()[0] == "3" and len(resp)>=3:
                print("Соединение установлено")
                client_sock.sendall("Hello".encode())
                time.sleep(2)

            elif resp.decode()[0] == "4" and len(resp)>=3:
                print("Что-то пошло не так")
                client_sock.close()
                return
    except:
        print("соединение разорвано")
if __name__ == '__main__':
    main()

    
