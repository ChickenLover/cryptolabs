#!/usr/bin/env python3

from math import ceil
import hashlib
import socket
from random import randrange, getrandbits
from sys import getsizeof

class Server():
    def __init__(self, port):
        self.sock = socket.socket()
        self.sock.bind(('', port))

    def listen(self, connections):
        while True:
            self.sock.listen(connections)
            conn, addr = self.sock.accept()
            print('connected:', addr)

            while True:
                data = conn.recv(1024)
                if not data:
                    break
                other_public_length = data[0]
                g_len = data[1]
                p_len = data[2]
                other_public = int.from_bytes(data[3:3 + other_public_length], "big")
                g = int.from_bytes(
                    data[3 + other_public_length: 3 + other_public_length + g_len], "big")
                print('\ng', g)
                p = int.from_bytes(
                    data[3 + other_public_length + g_len: 3 + other_public_length + g_len + p_len], "big")
                print('\np', p)
                private = randrange(2, pow(2, 127))
                public = pow(g, private, p)
                key = pow(other_public, private, p)
                print('\nkey', key)
                public_len = getsizeof(public)
                conn.send(bytes([public_len]) + public.to_bytes(public_len, "big"))


server = Server(9091)
server.listen(1)
