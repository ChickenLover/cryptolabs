from math import ceil
from random import randrange, getrandbits

from utils import is_prime, sha1, set_bit

def gen_pq(m, l):
    m1 = ceil(m / 160)
    l1 = ceil(l / 160)
    n1 = ceil(l / 1024)
    prime = False
    while not prime:
        seedlen = m + randrange(100)
        seed = getrandbits(seedlen)
        u = 0
        for i in range(m1):
            seed_i = seed + i
            sha1_seed_i = sha1(seed_i.to_bytes(ceil(seed_i.bit_length() / 8), "big"))
            seed_m1_i = (seed + i + m1) % pow(2, seedlen)
            sha1_seed_m1_i = sha1(seed_m1_i.to_bytes(ceil(seed_m1_i.bit_length() / 8), "big"))
            xor1 = bytes(a ^ b for a, b in zip(sha1_seed_i, sha1_seed_m1_i))
            u = u + (int.from_bytes(xor1, "big")) * pow(2, 160 * i)
        q = u % pow(2, m)#(2**m)
        # print('q1', q)
        q = set_bit(q, 0)
        # print('q2', q)
        q  = set_bit(q, q.bit_length() - 1)
        # print('q3', q)
        prime = is_prime(q)
    # print(q)
    counter = 0
    while True:
        r = l1 * counter + seed + 2 * m1
        v = 0
        for i in range(l1):
            r_i = r + i
            v = v + int.from_bytes(sha1(r_i.to_bytes(ceil(r_i.bit_length() / 8), "big")), "big") * pow(2, 160 * i)
        w = v % pow(2, l)
        x = w | pow(2, l - 1)
        p = x - (x % (2 * q)) + 1
        if p > pow(2, l-1) and is_prime(p):
            return (p, q, seed, counter)
        counter += 1
        if counter >= (4096 * n1):
            break
    return False

def gen_g(p, q):
    j = (p - 1) // q
    prev_h = []
    while True:
        h = randrange(2, p - 1)
        if h in prev_h:
            continue
        prev_h.append(h)
        g = pow(h, j, p)
        if g != 1:
            return g

def check_public_key(key, p, q):
    if key < 2 or key > p - 1:
        return False
    if pow(key, q, p) != 1:
        return False
    return True

def gen_keys():
    while True:
        p, q, seed, counter = gen_pq(128, 1024)
        g = gen_g(p, q)
        private = randrange(2, q - 2)
        public = pow(g, private, p)
        if check_public_key(public, p, q):
            break
    return (private, public, g, p)
