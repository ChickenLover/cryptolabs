#!/usr/bin/env python3

import socket
from random import randrange, getrandbits
from sys import getsizeof

from dh_keys import gen_keys

class Client():
    def __init__(self, server, port):
        self.sock = socket.socket()
        self.sock.connect((server, port))

    def dh(self):
        private, public, g, p = gen_keys()
        print('g', g)
        print('\np', p)
        public_len = getsizeof(public)
        g_len = getsizeof(g)
        p_len = getsizeof(p)
        packet = bytes([public_len, g_len, p_len]) + \
            public.to_bytes(public_len, "big") + \
            g.to_bytes(g_len, "big") + p.to_bytes(p_len, "big")
        self.sock.send(packet)
        data = self.sock.recv(1024)
        other_public_length = data[0]
        other_public = int.from_bytes(data[1: 1 + other_public_length], "big")
        key = pow(other_public, private, p)
        print('\nkey', key)


client = Client('localhost', 9091)
client.dh()
